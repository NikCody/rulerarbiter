package com.gmail.nikcody92.android.rulerfactory.rulerarbiter;

import android.graphics.Matrix;

import com.gmail.nikcody92.android.rulerfactory.resizablegauge.ResizableGauge;
import com.gmail.nikcody92.android.rulerfactory.resizablegauge.model.TransformRect;
import com.gmail.nikcody92.android.rulerfactory.touchdispatcher.observer.TouchObserver;

import org.bitbucket.nikcody92.scalableimageview.ScalableImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class RulerArbiter implements TouchObserver {
    private ScalableImageView imageView;
    private ArrayList<ResizableGauge> gauges;

    public RulerArbiter(ScalableImageView image, ResizableGauge... gauges) {
        this.imageView = image;
        this.gauges = new ArrayList<>(Arrays.asList(gauges));
    }

    @Override
    public boolean isTouched() {
        return false;
    }

    @Override
    public void handleDown(boolean b, int i, int i1) {

    }

    @Override
    public void doScroll(int i, int i1) {

    }

    @Override
    public void doLongPress(int i, int i1) {

    }

    @Override
    public void doFling(float v, float v1) {

    }

    @Override
    public void doScale(float factor) {
        ArrayList<TransformRect> areaList = new ArrayList<>();
        Matrix imageMatrix = new Matrix(), deltaMatrix = new Matrix();
        TransformRect gaugeArea;
        Iterator<TransformRect> areaIter;
        imageView.getImageMatrix().invert(imageMatrix);
        deltaMatrix.postTranslate(-imageView.getTranslationX() - imageView.getPaddingLeft(), -imageView.getTranslationY() - imageView.getPaddingTop());
        for (ResizableGauge gauge : gauges) {
            gaugeArea = TransformRect.applyTransform(gauge.getShapeArea(), deltaMatrix);
            gaugeArea = TransformRect.applyTransform(gaugeArea, imageMatrix);
            areaList.add(gaugeArea);
        }
        imageView.doScale(factor);
        imageMatrix = imageView.getImageMatrix();
        deltaMatrix = new Matrix();
        deltaMatrix.postTranslate(imageView.getTranslationX() + imageView.getPaddingLeft(), imageView.getTranslationY() + imageView.getPaddingTop());
        areaIter = areaList.iterator();
        for (ResizableGauge gauge : gauges) {
            gaugeArea = TransformRect.applyTransform(areaIter.next(), imageMatrix);
            gaugeArea = TransformRect.applyTransform(gaugeArea, deltaMatrix);
            gauge.setShapeArea(gaugeArea);
        }
    }
}
